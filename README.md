# Garden Center API Project 


## Project Background

 * This project uses a three layered architecture to build a Spring REST API. 

## Database Information

 * database name: POSTGRESQL
 * username: postgres
 * password: root
 * Port:5432

## Postman collection URL
 * https://api.postman.com/collections/25365335-e117400c-d1b1-4b11-b31f-5fa07183296f?access_key=PMAT-01GSPE5Z7DD23NZGYV754VSBMG

## Testing
***Mockito Unit Test:***
 * Look for src\test\java\com\churina\gardenCenter\services, right click and run test.
 * Code coverage can be viewed by right clicking on services package and select run with coverage.

***Integration Test:***
 * Look for src\test\java\com\churina\gardenCenter\controllers, right click and run test.
 * Code coverage can be viewed by right clicking on controllers package and select run with coverage.

## Logging

 * This project log information to console and file. 
 * Logging configuration can be checked in log4j2.xml file. 
 * Logger will log error scenarios to a file.

## Linting
 * For linting, please check Google Java Style Guide https://google.github.io/styleguide/javaguide.html
