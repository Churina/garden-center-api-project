package com.churina.gardenCenter.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import jakarta.validation.constraints.Pattern;
import org.hibernate.validator.constraints.Length;

import java.util.List;
import java.util.Objects;

import static com.churina.gardenCenter.constant.StringConstants.BAD_REQUEST_EMAIL;
import static com.churina.gardenCenter.constant.StringConstants.BAD_REQUEST_PASSWORD;


@Entity
@Table(name = "users")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private String title;
    private String[] roles;
    @Pattern(regexp = "^(.+)@(\\S+)$",message = BAD_REQUEST_EMAIL)
    private String email;
    @Length(min = 8, message = BAD_REQUEST_PASSWORD)
    private String password;

    public User() {
    }

    public User(String name, String title, String[] roles, String email, String password) {
        this.name = name;
        this.title = title;
        this.roles = roles;
        this.email = email;
        this.password = password;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String[] getRoles() {
        return roles;
    }

    public void setRoles(String[] roles) {
        this.roles = roles;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
