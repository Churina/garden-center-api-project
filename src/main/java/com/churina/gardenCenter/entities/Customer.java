package com.churina.gardenCenter.entities;

import jakarta.persistence.*;
import jakarta.validation.constraints.Pattern;



@Entity
@Table(name = "customers")
public class Customer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    @Pattern(regexp = "^(.+)@(\\S+)$")
    private String email;
    @OneToOne(cascade=CascadeType.ALL)
    private Address address;


    public Customer() {
    }

    public Customer(String name, String email,Address address) {
        this.name = name;
        this.email = email;
        this.address = address;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }
}
