package com.churina.gardenCenter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GardenCenterApplication {
	private static Logger logger = LoggerFactory.getLogger(GardenCenterApplication.class);
	public static void main(String[] args) {
		logger.trace("This is a trace message");
		logger.debug("This is a debugging message");
		logger.info("This is an information message");
		logger.warn("This is a warning message");
		logger.error("This is an error message");

		SpringApplication.run(GardenCenterApplication.class, args);

	}

}
