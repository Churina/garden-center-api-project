package com.churina.gardenCenter.repositories;

import com.churina.gardenCenter.entities.Address;
import com.churina.gardenCenter.entities.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CustomerRepository extends JpaRepository<Customer,Long> {
    Boolean existsByEmail(String email);
}
