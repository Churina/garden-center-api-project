package com.churina.gardenCenter.repositories;

import com.churina.gardenCenter.entities.Order;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OrderRepository extends JpaRepository<Order, Long> {

    List<Order> findByItems_ProductId(Long id);
    List<Order> findByItems_Quantity(Integer quantity);

}
