package com.churina.gardenCenter.repositories;

import com.churina.gardenCenter.entities.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {
    Boolean existsBySku (String sku);
}
