package com.churina.gardenCenter.constant;

public class StringConstants {
    public static final String SERVICE_UNAVAILABLE = "The database is not running, service is unavailable at the moment";
    public static final String NOT_FOUND = "Not Found";
    public static final String SERVER_ERROR = "Server Error";
    public static final String BAD_DATA = "Bad Data";
    public static final String EMAIL_CONFLICT = "The email address is already in use";
    public static final String BAD_REQUEST_EMAIL = "The email must have an @ symbol";
    public static final String BAD_REQUEST_PASSWORD = "The password must have at least 8 characters";
    public static final String BAD_REQUEST_ID = "The id must match id of path parameter";
    public static final String BAD_REQUEST_STATE = "The state must be valid 2 letter US state abbreviation";
    public static final String BAD_REQUEST_ZIPCODE = "The zip code format is XXXXX or XXXXX-XXXX";
    public static final String SKU_CONFLICT = "The sku is already in use";
    public static final String BAD_REQUEST_PRICE = "The price must have exactly 2 decimal places";
    public static final String BAD_REQUEST_ORDER_TOTAL = "The order total must have 2 decimal places";
    public static final String BAD_REQUEST_CUSTOMER_NOT_FOUND = "The customer does not exist in the database";
}