package com.churina.gardenCenter.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.rmi.ServerError;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.churina.gardenCenter.constant.StringConstants.*;

@ControllerAdvice
public class ExceptionController {

    @ExceptionHandler(ResourceNotFound.class)
    protected ResponseEntity<ExceptionResponse> resourceNotFound(ResourceNotFound exception) {
        ExceptionResponse response =
                new ExceptionResponse(NOT_FOUND, new Date(), exception.getMessage());

        return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(ServiceUnavailable.class)
    protected ResponseEntity<ExceptionResponse> serverError(ServiceUnavailable exception) {
        ExceptionResponse response =
                new ExceptionResponse(SERVER_ERROR, new Date(), exception.getMessage());

        return new ResponseEntity<>(response, HttpStatus.SERVICE_UNAVAILABLE);
    }

    @ExceptionHandler(BadDataResponse.class)
    protected ResponseEntity<ExceptionResponse> badDataResponse(BadDataResponse exception) {

        ExceptionResponse response =
                new ExceptionResponse(BAD_DATA, new Date(), exception.getMessage());

        return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(Conflict.class)
    protected ResponseEntity<ExceptionResponse> conflict(Conflict exception) {

        ExceptionResponse response =
                new ExceptionResponse(BAD_DATA, new Date(), exception.getMessage());

        return new ResponseEntity<>(response, HttpStatus.CONFLICT);
    }
}
