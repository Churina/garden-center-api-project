package com.churina.gardenCenter.exceptions;

public class Conflict extends RuntimeException {

    public Conflict() {
    }

    public Conflict(String message) {
        super(message);
    }
}
