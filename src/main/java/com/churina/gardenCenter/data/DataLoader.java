package com.churina.gardenCenter.data;

import com.churina.gardenCenter.entities.*;
import com.churina.gardenCenter.repositories.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.sql.Date;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Arrays;


@Component
public class DataLoader implements CommandLineRunner {
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private AddressRepository addressRepository;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private ItemRepository itemRepository;


    private User user1;
    private User user2;
    private User user3;


    private Customer customer1;
    private Customer customer2;
    private Customer customer3;
    private Customer customer4;
    private Customer customer5;


    private Product product1;
    private Product product2;
    private Product product3;
    private Product product4;


    private Order order1;
    private Order order2;
    private Order order3;
    private Order order4;


    private Items item1;
    private Items item2;
    private Items item3;
    private Items item4;
    private Items item5;
    private Items item6;


    @Override
    public void run(String... args) throws Exception {
        loadUsers();
        loadCustomers();
        loadProducts();
        loadOrders();
        loadItems();
    }


    private void loadUsers(){
        user1 = userRepository.save(new User("Alice","Manager",  new String[]{"ADMIN"},"alice@gmail.com","abc12345"));
        user2 = userRepository.save(new User("Bob","Developer", new String[]{"EMPLOYEE"},"bob@gmail.com","def12345"));
        user3 = userRepository.save(new User("Coco","Developer", new String[]{"EMPLOYEE"},"coco@gmail.com","ghi12345"));
    }


    private void loadCustomers(){
        customer1 = customerRepository.save(new Customer("David","david@gmail.com",new Address("123 Street","Charlotte","NC","12345")));
        customer2 = customerRepository.save(new Customer("Emily","emily@gmail.com",new Address("456 Street","Columbia","SC","56789")));
        customer3 = customerRepository.save(new Customer("Frank","frank@gmail.com",new Address("789 Street","Atlanta","GA","34567")));
        customer4 = customerRepository.save(new Customer("Billy","billy@gmail.com",new Address("123 Street","Atlanta","GA","23456")));
        customer5 = customerRepository.save(new Customer("Buck","buck@gmail.com",new Address("789 Street","Charlotte","NC","12345")));
    }


    private void loadProducts(){
        product1 = productRepository.save(new Product("a12345xyz","type1","name1","description1","manufacturer1", BigDecimal.valueOf(19.99)));
        product2 = productRepository.save(new Product("b23456opt","type2","name2","description2","manufacturer2", BigDecimal.valueOf(29.99)));
        product3 = productRepository.save(new Product("c34567abc","type3","name3","description3","manufacturer3", BigDecimal.valueOf(39.99)));
        product4 = productRepository.save(new Product("d45678opt","type4","name4","description4","manufacturer4", BigDecimal.valueOf(49.99)));
    }


    private void loadOrders(){

        order1 = orderRepository.save(new Order(
                customer3.getId(),
                Date.from(LocalDate.parse("2023-10-17").atStartOfDay(ZoneId.systemDefault()).toInstant()),
                BigDecimal.valueOf(199.98)));
        order2 = orderRepository.save(new Order(
                customer2.getId(),
                Date.from(LocalDate.parse("2022-11-19").atStartOfDay(ZoneId.systemDefault()).toInstant()),
                BigDecimal.valueOf(299.98)));
        order3 = orderRepository.save(new Order(
                customer1.getId(),
                Date.from(LocalDate.parse("2021-09-22").atStartOfDay(ZoneId.systemDefault()).toInstant()),
                BigDecimal.valueOf(399.98)));
        order4 = orderRepository.save(new Order(
                customer1.getId(),
                Date.from(LocalDate.parse("2020-01-01").atStartOfDay(ZoneId.systemDefault()).toInstant()),
                BigDecimal.valueOf(499.98)));
    }


    private void loadItems(){
        item1 = itemRepository.save(new Items(product1.getId(),15,order1));
        item2 = itemRepository.save(new Items(product2.getId(),3,order1));
        item3 = itemRepository.save(new Items(product3.getId(),2,order3));
        item4 = itemRepository.save(new Items(product2.getId(),12,order2));
        item5 = itemRepository.save(new Items(product1.getId(),10,order3));
        item6 = itemRepository.save(new Items(product4.getId(),8,order4));
    }

}
