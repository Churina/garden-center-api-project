package com.churina.gardenCenter.controllers;

import com.churina.gardenCenter.entities.Customer;
import com.churina.gardenCenter.entities.Order;
import com.churina.gardenCenter.services.OrderService;
import jakarta.validation.Valid;
import org.aspectj.weaver.ast.Or;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/orders")
public class OrderController {

    @Autowired
    private OrderService orderService;

    @GetMapping
    public List<Order> getAllOrders(Order order) throws Exception{

        return orderService.getAllOrders(order);
    }


    @GetMapping(params = "productId")
    public ResponseEntity<List<Order>> getOrdersByProductId(Long productId){
        return new ResponseEntity<>(orderService.getOrdersByProductId(productId), HttpStatus.OK);
    }


    @GetMapping(params = "quantity")
    public ResponseEntity<List<Order>> getOrderByQuantity(Integer quantity){
        return new ResponseEntity<>(orderService.getOrdersByQuantity(quantity),HttpStatus.OK);
    }


    @GetMapping(value = "/{id}")
    public ResponseEntity<Order> getOrderById(@PathVariable Long id){
        return new ResponseEntity<>(orderService.getOrderById(id),HttpStatus.OK);
    }


    @PostMapping
    public ResponseEntity<Order> createOrder(@Valid @RequestBody Order order){
        return new ResponseEntity<>(orderService.addOrder(order),HttpStatus.CREATED);
    }

    @PutMapping(value = "/{id}")
    public ResponseEntity<Order> updateOrderById(@PathVariable Long id, @RequestBody Order order){
        return new ResponseEntity<>(orderService.updateOrderById(id, order), HttpStatus.OK);
    }


    @DeleteMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteOrderById(@PathVariable Long id){
        orderService.deleteOrderById(id);
    }

//    public ResponseEntity<Order> deleteOrderById(@PathVariable Long id){
//        orderService.deleteOrderById(id);
//        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
//    }

}
