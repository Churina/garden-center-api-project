package com.churina.gardenCenter.controllers;

import com.churina.gardenCenter.entities.Product;
import com.churina.gardenCenter.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/products")
public class ProductController {
    @Autowired
    private ProductService productService;


   @GetMapping
    public ResponseEntity<List<Product>> getAllProduct(Product product){
        return new ResponseEntity<>(productService.getAllProducts(product), HttpStatus.OK);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<Product> getProductById(@PathVariable Long id){
       return new ResponseEntity<>(productService.getProductById(id), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<Product> createProduct(@RequestBody Product product){
       return new ResponseEntity<>(productService.addProduct(product),HttpStatus.CREATED);
    }

    @PutMapping(value = "/{id}")
    public ResponseEntity<Product> updateProductById(@PathVariable Long id, @RequestBody Product product){
       return new ResponseEntity<>(productService.updateProductById(id, product), HttpStatus.OK);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Product> deleteProductById(@PathVariable Long id){
       productService.deleteProductById(id);
       return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
