package com.churina.gardenCenter.controllers;

import com.churina.gardenCenter.entities.User;
import com.churina.gardenCenter.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import java.util.List;


/**
 * This controller holds CRUD methods for the User entity
 */
@RestController
@RequestMapping("/users")
public class UserController {

    @Autowired
    private UserService userService;


    @GetMapping
    public List<User> getAllUsers(User user) throws Exception{

        return userService.getAllUsers(user);
}


    @GetMapping(value = "/{id}")
    public User getUserById(@PathVariable Long id) throws Exception{
        return userService.getUserById(id);
    }


    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public User createUser(@RequestBody User user){

        return userService.addUser(user);
    }


    @PutMapping(value = "/{id}")
    public User updateUserById(@PathVariable Long id, @RequestBody User user){
        return userService.updateUserById(id, user);
    }


    @DeleteMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteUserById(@PathVariable Long id){

        userService.deleteUserById(id);
    }

}
