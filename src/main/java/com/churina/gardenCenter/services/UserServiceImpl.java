package com.churina.gardenCenter.services;

import com.churina.gardenCenter.entities.User;
import com.churina.gardenCenter.exceptions.BadDataResponse;
import com.churina.gardenCenter.exceptions.ResourceNotFound;
import com.churina.gardenCenter.exceptions.ServiceUnavailable;
import com.churina.gardenCenter.exceptions.Conflict;
import com.churina.gardenCenter.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;
import java.util.List;

import static com.churina.gardenCenter.constant.StringConstants.*;

@Service
public class UserServiceImpl implements UserService{
    @Autowired
    private UserRepository userRepository;


    /**
     * Reads all Users from the database.
     *
     * @Param user
     * @return - a list of all Users or an empty array with a 200 code if no user exist.
     */

    @Override
    public List<User> getAllUsers(User user){
        try {
            if(user.equals(null)){
                return  userRepository.findAll();
            } else {
                Example<User> userExample = Example.of(user);
                return userRepository.findAll(userExample);
            }
        } catch (Exception e){
            throw new ServiceUnavailable(e);
        }
    }


    /**
     * Looks up a user from the database by id.
     *
     * @param id - the id of the users to lookup.
     * @return - the user belonging to the ID or a 404 if not found.
     */

    @Override
    public User getUserById(Long id){
        try {
            User userLookUpResult = userRepository.findById(id).orElse(null);
            if(userLookUpResult != null){
                return userLookUpResult;
            }
        } catch (Exception e) {
            throw new ServiceUnavailable(e);
        }
        throw new ResourceNotFound("Could not locate a User with the id: " + id);
    }

    /**
     * add a new user
     *
     * @param user
     * @return new user with a status code 201.If email already exist,return error with a 409.
     * if invalid user has been provided, return error with 400.
     */

    @Override
    public User addUser(User user){

        try {
            // check if email already exists
            boolean emailAlreadyExists = userRepository.existsByEmail(user.getEmail());
            if (!emailAlreadyExists) {
                return userRepository.save(user);
            }
        } catch (Exception e) {
            // if email or password is invalid, throw error
            throw new BadDataResponse(BAD_REQUEST_EMAIL + "; " + BAD_REQUEST_PASSWORD);
        }
        // if made it to this point, email is not unique
        throw new Conflict(EMAIL_CONFLICT);
    }

    /** update user by id.
     *
     * @param id
     * @param user
     * @return user will be updated with code 200. if given no existent id, throw error with 404.
     * if given email is same as different user's email, throw error 409.
     * if given invalid user, throw error 400
     */

    @Override
    public User updateUserById(Long id, User user){

        User userLookUpResult;
        // check if id  doesn't match, throw error 400
        if(!user.getId().equals(id)){
            throw new BadDataResponse(BAD_REQUEST_ID);
        }
        try {
            userLookUpResult = userRepository.findById(id).orElse(null);
            if(userLookUpResult != null){
                // if the given email is not changed or given email is not same as different user's email, update this user
                if(userLookUpResult.getEmail().equals(user.getEmail()) || !userRepository.existsByEmail(user.getEmail())){
                    return userRepository.save(user);
                }
            }
        } catch (Exception e) {
            // if given invalid email and password, throw error 400
            throw new BadDataResponse(BAD_REQUEST_EMAIL + "; " + BAD_REQUEST_PASSWORD);
        }

        if(userLookUpResult == null){
            // if given no existent id, throw error 404
            throw new ResourceNotFound("Could not locate a User with the id: " + id);
        } else {
            // if given email is same as different user's email, throw error 409
            throw new Conflict(EMAIL_CONFLICT);
        }
    }

    /** delete user by id
     *
     * @param id
     * if given no existent id, throw error with 404
     */

    @Override
    public void deleteUserById(Long id){
        try {
            if(userRepository.existsById(id)){
                userRepository.deleteById(id);
                return;
            }
        } catch (Exception e) {
            throw new ServiceUnavailable(e);
        }
            throw new ResourceNotFound("Could not locate a User with the id: " + id);
    }
}
