package com.churina.gardenCenter.services;

import com.churina.gardenCenter.entities.Address;
import com.churina.gardenCenter.entities.Customer;

import java.util.List;

public interface CustomerService {
    List<Customer> getAllCustomers(Customer customer);
    Customer getCustomerById(Long id);
    Customer addCustomer(Customer customer);
    Customer updateCustomerById(Long id, Customer customer);
    void deleteCustomerById(Long id);

}
