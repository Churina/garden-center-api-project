package com.churina.gardenCenter.services;

import com.churina.gardenCenter.entities.User;

import java.util.List;
import java.util.Optional;

public interface UserService {
    List<User> getAllUsers(User user);
    User getUserById(Long id);
    User addUser(User user);
    User updateUserById(Long id, User user);
    void deleteUserById(Long id);
}
