package com.churina.gardenCenter.services;

import com.churina.gardenCenter.entities.Product;
import com.churina.gardenCenter.exceptions.BadDataResponse;
import com.churina.gardenCenter.exceptions.Conflict;
import com.churina.gardenCenter.exceptions.ResourceNotFound;
import com.churina.gardenCenter.exceptions.ServiceUnavailable;
import com.churina.gardenCenter.repositories.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;
import java.util.List;

import static com.churina.gardenCenter.constant.StringConstants.*;

@Service
public class ProductServiceImpl implements ProductService{

    @Autowired
    private ProductRepository productRepository;



    @Override
    public List<Product> getAllProducts(Product product){
        try {
            if(product.equals(null)){
                return productRepository.findAll();
            } else {
                Example<Product> productExample = Example.of(product);
                return productRepository.findAll(productExample);
            }
        } catch (Exception e) {
            throw new ServiceUnavailable(e);
        }
    }


    @Override
    public Product getProductById(Long id){
        try{
            Product productLookUpResult = productRepository.findById(id).orElse(null);
            if(productLookUpResult != null){
                return productLookUpResult;
            }
        } catch (Exception e){
            throw new ServiceUnavailable(e);
        }
        throw new ResourceNotFound("Could not locate a product with the id: " + id);
    }


    @Override
    public Product addProduct(Product product){

        // check if price is valid
        if(product.getPrice().scale() != 2){
            throw new BadDataResponse(BAD_REQUEST_PRICE);
        }

        try{
            // check if sku already exists
            boolean skuAlreadyExists = productRepository.existsBySku(product.getSku());

            if(!skuAlreadyExists){
                return productRepository.save(product);
            }
        } catch (Exception e) {
            throw new ServiceUnavailable(e);
        }
        // if made it to this point, sku is not unique
        throw new Conflict(SKU_CONFLICT);
    }


    @Override
    public Product updateProductById(Long id, Product product){
        Product productLookupResult;

        // check if id doesn't match, throw error 400
        if(!product.getId().equals(id)){
            throw new BadDataResponse(BAD_REQUEST_ID);
        }

        // check if price is valid
        if(product.getPrice().scale() != 2){
            throw new BadDataResponse(BAD_REQUEST_PRICE);
        }

        try{
           productLookupResult = productRepository.findById(id).orElse(null);
           if(productLookupResult != null){
               // if the given sku is not changed or given sku is not same as different product's sku, update this product
               if(productLookupResult.getSku().equals(product.getSku()) || !productRepository.existsBySku(product.getSku())){
                   return productRepository.save(product);
               }
           }
        } catch (Exception e) {
            // if given invalid price,throw 400
            throw new BadDataResponse(BAD_REQUEST_PRICE);
        }

        if(productLookupResult == null){
            // if given no existent id, throw error 404
            throw new ResourceNotFound("Could not locate a product with the id: " + id);
        } else {
            // if given sku is same as different product's sku, throw error 409
            throw new Conflict(SKU_CONFLICT);
        }
    }



    @Override
    public void deleteProductById(Long id){
        try{
            if(productRepository.existsById(id)){
                productRepository.deleteById(id);
                return;
            }
        } catch (Exception e) {
            throw new ServiceUnavailable(e);
        }
        throw new ResourceNotFound("Could not locate a product with the id: " + id);
    }














}
