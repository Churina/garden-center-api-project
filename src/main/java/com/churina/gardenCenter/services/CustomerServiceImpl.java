package com.churina.gardenCenter.services;

import com.churina.gardenCenter.entities.Address;
import com.churina.gardenCenter.entities.Customer;
import com.churina.gardenCenter.exceptions.BadDataResponse;
import com.churina.gardenCenter.exceptions.Conflict;
import com.churina.gardenCenter.exceptions.ResourceNotFound;
import com.churina.gardenCenter.exceptions.ServiceUnavailable;
import com.churina.gardenCenter.repositories.AddressRepository;
import com.churina.gardenCenter.repositories.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;
import java.util.List;

import static com.churina.gardenCenter.constant.StringConstants.*;
import static java.util.Collections.addAll;

@Service
public class CustomerServiceImpl implements CustomerService{

    @Autowired
    private CustomerRepository customerRepository;
    @Autowired
    private AddressRepository addressRepository;


    /**
     * get all customers
     * @param customer
     * @return list of customers
     */

    @Override
    public List<Customer> getAllCustomers(Customer customer){
        try{
            if(customer.equals(null)){
                return customerRepository.findAll();
            } else {
                Example<Customer> customerExample = Example.of(customer);
                return customerRepository.findAll(customerExample);
            }
        } catch (Exception e){
            throw new ServiceUnavailable(e);
        }
    }


    /**
     * Looks up a customer from the database by id.
     *
     * @param id - the id of the customers to lookup.
     * @return - the customer belonging to the ID or a 404 if not found.
     */

    @Override
    public Customer getCustomerById(Long id){
        try{
            Customer customerLookUpResult = customerRepository.findById(id).orElse(null);
            if(customerLookUpResult != null){
                return customerLookUpResult;
            }
        } catch (Exception e){
            throw new ServiceUnavailable(e);
        }
        throw new ResourceNotFound("Could not locate a customer with the id: " + id);
    }

    /**
     * add a new customer
     *
     * @param customer
     * @return new customer with a status code 201.If email already exist,return error with a 409.
     * if invalid customer has been provided, return error with 400.
     */

    @Override
    public Customer addCustomer(Customer customer){

        try {
            // check if email already exists
            boolean emailAlreadyExists = customerRepository.existsByEmail(customer.getEmail());
            if (!emailAlreadyExists){
                return customerRepository.save(customer);
            }
        } catch (Exception e){
            // if email or password is invalid, throw error
            throw new BadDataResponse(BAD_REQUEST_EMAIL + "; " + BAD_REQUEST_STATE + "; " + BAD_REQUEST_ZIPCODE);
        }
        // if made it to this point, email is not unique
        throw new Conflict(EMAIL_CONFLICT);
    }

    /** update customer by id.
     *
     * @param id
     * @param customer
     * @return customer will be updated with code 200. if given no existent id, throw error with 404.
     * if given email is same as different customer's email, throw error 409.
     * if given invalid customer, throw error 400
     */

    @Override
    public Customer updateCustomerById(Long id, Customer customer){
        Customer customerLookupResult;
        // check if id  doesn't match, throw error 400
        if(!customer.getId().equals(id)){
            throw new BadDataResponse(BAD_REQUEST_ID);
        }
        try {
            customerLookupResult = customerRepository.findById(id).orElse(null);
            if(customerLookupResult != null){
                // if the given email is not changed or given email is not same as different user's email, update this customer
                if(customerLookupResult.getEmail().equals(customer.getEmail()) || !customerRepository.existsByEmail(customer.getEmail())){
                    return customerRepository.save(customer);
                }
            }
        } catch (Exception e) {
            // if given invalid email and password, throw error 400
            throw new BadDataResponse(BAD_REQUEST_EMAIL + "; " + BAD_REQUEST_STATE + "; " + BAD_REQUEST_ZIPCODE);
        }

        if(customerLookupResult == null){
            // if given no existent id, throw error 404
            throw new ResourceNotFound("Could not locate a customer with the id: " + id);
        } else {
            // if given email is same as different user's email, throw error 409
            throw new Conflict(EMAIL_CONFLICT);
        }
    }


    /** delete customer by id
     *
     * @param id
     * if given no existent id, throw error with 404
     */

    @Override
    public void deleteCustomerById(Long id){
        try{
            if(customerRepository.existsById(id)){
                customerRepository.deleteById(id);
                return;
            }
        } catch (Exception e){
            throw new ServiceUnavailable(e);
        }
        throw new ResourceNotFound("Could not locate a customer with the id: " + id);
    }
}
