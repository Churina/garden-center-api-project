package com.churina.gardenCenter.services;

import com.churina.gardenCenter.entities.Product;

import java.util.List;

public interface ProductService {
    List<Product> getAllProducts(Product product);
    Product getProductById(Long id);
    Product addProduct(Product product);
    Product updateProductById(Long id, Product product);
    void deleteProductById(Long id);
}
