package com.churina.gardenCenter.services;

import com.churina.gardenCenter.entities.Items;
import com.churina.gardenCenter.entities.Order;
import com.churina.gardenCenter.entities.Product;
import com.churina.gardenCenter.exceptions.BadDataResponse;
import com.churina.gardenCenter.exceptions.ResourceNotFound;
import com.churina.gardenCenter.exceptions.ServiceUnavailable;
import com.churina.gardenCenter.repositories.CustomerRepository;
import com.churina.gardenCenter.repositories.ItemRepository;
import com.churina.gardenCenter.repositories.OrderRepository;
import com.churina.gardenCenter.repositories.ProductRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;
import java.util.List;

import static com.churina.gardenCenter.constant.StringConstants.*;

@Service
public class OrderServiceImpl implements OrderService{


    @Autowired
    private OrderRepository orderRepository;
    @Autowired
    private CustomerRepository customerRepository;
    @Autowired
    private ProductRepository productRepository;
    @Autowired
    private ItemRepository itemRepository;


    @Override
    public List<Order> getAllOrders(Order order){

        try{
            if(order.equals(null)){
                return orderRepository.findAll();
            } else {
                Example<Order> orderExample = Example.of(order);
                return orderRepository.findAll(orderExample);
            }
        } catch (Exception e){
            throw new ServiceUnavailable(e);
        }
    }

    public List<Order> getOrdersByProductId(Long productId){

        return orderRepository.findByItems_ProductId(productId);
    }


    public List<Order> getOrdersByQuantity(Integer quantity){

        return orderRepository.findByItems_Quantity(quantity);
    }


    @Override
    public Order getOrderById(Long id){
        try{
            Order orderLookUpResult = orderRepository.findById(id).orElse(null);
            if(orderLookUpResult != null){
                return orderLookUpResult;
            }
        } catch (Exception e){
            throw new ServiceUnavailable(e);
        }
        throw new ResourceNotFound("Could not locate a order with the id: " + id);
    }

    @Override
    public Order addOrder(Order order){

        // check if order total has exactly 2 decimal places
        if (order.getOrderTotal().scale() != 2) {
            throw new BadDataResponse(BAD_REQUEST_ORDER_TOTAL);
        }


        //check if customer exist in the database
        if (!customerRepository.existsById(order.getCustomerId())){
            throw new ResourceNotFound(BAD_REQUEST_CUSTOMER_NOT_FOUND);
        }

        for (Items item : order.getItems()) {
            item.setOrder(order);
        }

        try{
            return orderRepository.save(order);
        } catch (Exception e) {
            throw new ServiceUnavailable(e);
        }
    }



    @Override
    public Order updateOrderById(Long id, Order order){

        Order orderLookupResult;

        if(!order.getId().equals(id)){
            throw new BadDataResponse(BAD_REQUEST_ID);
        }


        // check if order total has exactly 2 decimal places
        if (order.getOrderTotal().scale() != 2) {
            throw new BadDataResponse(BAD_REQUEST_ORDER_TOTAL);
        }


        try{
            orderLookupResult = orderRepository.findById(id).orElse(null);
        } catch (Exception e){
            throw new ServiceUnavailable(e);
        }


        if(orderLookupResult != null){
            return orderRepository.save(order);
        } else {
            throw new ResourceNotFound("Could not locate a order with the id: " + id);
        }
    }


    @Override
    public void deleteOrderById(Long id){
        try{
            if (orderRepository.existsById(id)) {
                //itemRepository.deleteById(id);
                orderRepository.deleteById(id);
                return;
            }
        } catch (Exception e){
            throw new BadDataResponse("invalid");
        }

        throw new ResourceNotFound("Could not locate a order with the id: " + id);
    }

}
