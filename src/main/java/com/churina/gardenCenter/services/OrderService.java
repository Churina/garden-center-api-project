package com.churina.gardenCenter.services;

import com.churina.gardenCenter.entities.Order;
import org.aspectj.weaver.ast.Or;

import java.util.List;

public interface OrderService {
    List<Order> getAllOrders(Order order);
    List<Order> getOrdersByProductId(Long productId);
    List<Order> getOrdersByQuantity(Integer quantity);
    Order getOrderById(Long id);
    Order addOrder(Order order);
    Order updateOrderById(Long id, Order order);
    void deleteOrderById(Long id);

}