package com.churina.gardenCenter.controllers;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.DefaultMockMvcBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;


@SpringBootTest
@AutoConfigureMockMvc
class OrderControllerTest {

    ResultMatcher okStatus = MockMvcResultMatchers.status().isOk();
    ResultMatcher createdStatus = MockMvcResultMatchers.status().isCreated();
    ResultMatcher deletedStatus = MockMvcResultMatchers.status().isNoContent();
    ResultMatcher notFoundStatus = MockMvcResultMatchers.status().isNotFound();
    ResultMatcher badRequestStatus = MockMvcResultMatchers.status().isBadRequest();
    ResultMatcher dataErrorStatus = MockMvcResultMatchers.status().isServiceUnavailable();
    ResultMatcher serverErrorStatus = MockMvcResultMatchers.status().isInternalServerError();
    ResultMatcher expectedType = MockMvcResultMatchers.content()
            .contentType(MediaType.APPLICATION_JSON);

    @Autowired
    private WebApplicationContext wac;
    private MockMvc mockMvc;

    @BeforeEach
    public void setUp(){

        DefaultMockMvcBuilder builder = MockMvcBuilders.webAppContextSetup(this.wac);
        this.mockMvc = builder.build();
    }


    @Test
    public void getAllOrdersReturnsAtLeastTwo() throws Exception {
        mockMvc
                .perform(get("/orders"))
                .andExpect(okStatus)
                .andExpect(expectedType)
                .andExpect(jsonPath("$", hasSize(greaterThan(2))));
    }

    @Test
    public void getOrdersByProductId() throws Exception {
        mockMvc
                .perform(get("/orders?productId=1"))
                .andExpect(okStatus)
                .andExpect(expectedType);
    }

    @Test
    public void getOrdersByQuantity() throws Exception {
        mockMvc
                .perform(get("/orders?quantity=12"))
                .andExpect(okStatus)
                .andExpect(expectedType);
    }

    @Test
    public void getOrderThatDoesExistById()  throws Exception {
        mockMvc
                .perform(get("/orders/2"))
                .andExpect(okStatus)
                .andExpect(expectedType)
                .andExpect( jsonPath("$.orderTotal", is(299.98)));
    }


    @Test
    public void saveNewOrder() throws Exception {

        // get order2
        String order2 =
                mockMvc
                        .perform(get("/orders/2"))
                        .andExpect(okStatus)
                        .andExpect(expectedType)
                        .andReturn()
                        .getResponse()
                        .getContentAsString();


        // change date
        order2 = order2.replace("2022-11-19","2022-12-30");

        // add new order
        this.mockMvc
                .perform(post("/orders")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(order2))
                .andExpect(createdStatus)
                .andExpect(expectedType)
                .andExpect(jsonPath("$.date", is("2022-12-30")));
    }


    @Test
    public void updateOrderById() throws Exception {
        // get order1
        String order1 =
                mockMvc
                        .perform(get("/orders/1"))
                        .andExpect(okStatus)
                        //.andExpect(expectedType)
                        .andReturn()
                        .getResponse()
                        .getContentAsString();


        // change date
        order1 = order1.replace("2023-10-17","2022-11-27");

        // update order1
        this.mockMvc
                .perform(put("/orders/1")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(order1))
                .andExpect(okStatus)
                .andExpect(expectedType)
                .andExpect(jsonPath("$.date", is("2022-11-27")));
    }

    @Test
    public void deleteOrder() throws Exception {
        mockMvc
                .perform(delete("/orders/4"))
                .andExpect(deletedStatus);
    }


}