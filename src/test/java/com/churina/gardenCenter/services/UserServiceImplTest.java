package com.churina.gardenCenter.services;

import com.churina.gardenCenter.entities.User;
import com.churina.gardenCenter.exceptions.BadDataResponse;
import com.churina.gardenCenter.exceptions.Conflict;
import com.churina.gardenCenter.exceptions.ResourceNotFound;
import com.churina.gardenCenter.exceptions.ServiceUnavailable;
import com.churina.gardenCenter.repositories.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Example;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

class UserServiceImplTest {

    @Mock
    UserRepository mockUserRepository;

    @InjectMocks
    UserServiceImpl userServiceImpl;

    // member variables for expected results
    User testUser;
    List<User> testList = new ArrayList<>();

    @BeforeEach
    public void setUp(){
        // initialize mocks
        MockitoAnnotations.openMocks(this);

        //set up test user
        testUser = new User("Alice","Manager",  new String[]{"ADMIN"},"alice@gmail.com","abc12345");
        testUser.setId(1L);
        testList.add(testUser);

        when(mockUserRepository.findById(any(Long.class))).thenReturn(Optional.of(testList.get(0)));
        when(mockUserRepository.findAll()).thenReturn(testList);
        when(mockUserRepository.findAll(any(Example.class))).thenReturn(testList);
        when(mockUserRepository.saveAll(anyCollection())).thenReturn(testList);
        when(mockUserRepository.save(any(User.class))).thenReturn(testList.get(0));
    }

    @Test
    public void getAllUsers() {
        List<User> result = userServiceImpl.getAllUsers(new User());
        assertEquals(testList,result);
    }


    @Test
    public void getAllUsersWithExample(){
        List<User> result = userServiceImpl.getAllUsers(testUser);
        assertEquals(testList,result);
    }


    @Test
    public void getAllUsersUnexpectedError() {
        when(mockUserRepository.findAll()).thenThrow();
        assertThrows(ServiceUnavailable.class,() -> userServiceImpl.getAllUsers(new User()));
    }


    @Test
    public void getUsersById(){
        User result = userServiceImpl.getUserById(1L);
        assertEquals(testUser,result);
    }

    @Test
    public void getUserDBError(){
        when(mockUserRepository.findById(anyLong())).thenThrow(EmptyResultDataAccessException.class);
        assertThrows(ServiceUnavailable.class,() -> userServiceImpl.getUserById(1L));
    }


    @Test
    public void getUserNotFound(){
        when(mockUserRepository.findById(any(Long.class))).thenReturn(Optional.empty());
        assertThrows(ResourceNotFound.class,() -> userServiceImpl.getUserById(1L));
    }


    @Test
    public void addUser(){
        User result = userServiceImpl.addUser(testUser);
        assertEquals(testUser,result);
    }

    @Test
    public void addUserEmailConflict(){
        when(mockUserRepository.existsByEmail(any(String.class))).thenReturn(true);
        assertThrows(Conflict.class,() -> userServiceImpl.addUser(testUser));
    }


    @Test
    public void addUserDBError(){
        when(mockUserRepository.save(any(User.class))).thenThrow(BadDataResponse.class);
        assertThrows(BadDataResponse.class,() -> userServiceImpl.addUser(testUser));
    }


    @Test
    public void updateUserById(){
        User result = userServiceImpl.updateUserById(1L,testUser);
        assertEquals(testUser,result);
    }

    @Test
    public void updateUserEmailConflict(){
        when(mockUserRepository.existsByEmail(any(String.class))).thenReturn(true);
        User testUser = new User("Alice","Manager", new String[]{"ADMIN"},"bob@gmail.com","abc12345");
        testUser.setId(1L);
        assertThrows(Conflict.class,() -> userServiceImpl.updateUserById(1L, testUser));
    }

    @Test
    public void updateUserByIdDBError(){
        when(mockUserRepository.save(any(User.class))).thenThrow(BadDataResponse.class);
        assertThrows(BadDataResponse.class,() -> userServiceImpl.updateUserById(1L, testUser));
    }

    @Test
    public void updateUserByIdNotFound(){
        when(mockUserRepository.findById(anyLong())).thenReturn(Optional.empty());
        assertThrows(ResourceNotFound.class,() -> userServiceImpl.updateUserById(1L, testUser));
    }

    @Test
    public void updateUserBadData(){
        when(mockUserRepository.save(any(User.class))).thenReturn(testUser);
        assertThrows(BadDataResponse.class,() -> userServiceImpl.updateUserById(2L,testUser));
    }

    @Test
    public void deleteUser(){
        when(mockUserRepository.existsById(anyLong())).thenReturn(true);
        userServiceImpl.deleteUserById(1L);
        verify(mockUserRepository).deleteById(any());
    }

    @Test
    public void deleteUserDBError(){
        when(mockUserRepository.existsById(anyLong())).thenReturn(true);
        doThrow(ServiceUnavailable.class).when(mockUserRepository).deleteById(anyLong());
        assertThrows(ServiceUnavailable.class,() -> userServiceImpl.deleteUserById(1L));
    }

    @Test
    public void deleteUserNotFound(){
        when(mockUserRepository.existsById(anyLong())).thenReturn(false);
        assertThrows(ResourceNotFound.class,() -> userServiceImpl.deleteUserById(1L));
    }

}