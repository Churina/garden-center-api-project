package com.churina.gardenCenter.services;

import com.churina.gardenCenter.entities.*;
import com.churina.gardenCenter.exceptions.BadDataResponse;
import com.churina.gardenCenter.exceptions.ResourceNotFound;
import com.churina.gardenCenter.exceptions.ServiceUnavailable;
import com.churina.gardenCenter.repositories.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.exceptions.misusing.UnfinishedStubbingException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Example;
import org.springframework.transaction.CannotCreateTransactionException;

import java.math.BigDecimal;
import java.sql.Date;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

class OrderServiceImplTest {

    @Mock
    OrderRepository mockOrderRepository;
    @Mock
    CustomerRepository mockCustomerRepository;
    @Mock
    ProductRepository mockProductRepository;
    @Mock
    ItemRepository mockItemRepository;



    @InjectMocks
    OrderServiceImpl orderServiceImpl;

    // member variables for expected results
    Order testOrder;
    List<Order> testList = new ArrayList<>();


    @BeforeEach
    public void setUp(){
        // initialize mocks
        MockitoAnnotations.openMocks(this);

        //set up test user

        Address testAddress = new Address("123 street", "Charlotte","NC","12333");
        testAddress.setId(1L);
        Customer testCustomer = new Customer("Oliva","olova@gmail.com",testAddress);
        testCustomer.setId(1L);
        Product testProduct = new Product("sku12345","type","name","description","manufacturer",BigDecimal.valueOf(399.99));
        testProduct.setId(1L);
        Items testItem = new Items(testProduct.getId(),10,testOrder);
        testItem.setId(1L);
        List<Items> listItems = new ArrayList<>();
        listItems.add(testItem);
        testOrder = new Order(testCustomer.getId(),
                Date.from(LocalDate.parse("2020-01-01").atStartOfDay(ZoneId.systemDefault()).toInstant()),
                BigDecimal.valueOf(999.98));
        testOrder.setId(1L);
        testOrder.setItems(listItems);
        testList.add(testOrder);

        when(mockOrderRepository.findById(any(Long.class))).thenReturn(Optional.of(testList.get(0)));
        when(mockOrderRepository.findAll()).thenReturn(testList);
        when(mockOrderRepository.findAll(any(Example.class))).thenReturn(testList);
        when(mockOrderRepository.saveAll(anyCollection())).thenReturn(testList);
        when(mockOrderRepository.save(any(Order.class))).thenReturn(testList.get(0));

        when(mockCustomerRepository.existsById(any(Long.class))).thenReturn(true);
        when(mockOrderRepository.findByItems_ProductId(any(Long.class))).thenReturn(testList);
        when(mockOrderRepository.findByItems_Quantity(any(Integer.class))).thenReturn(testList);
    }


    @Test
    public void getAllOrders(){
        List<Order> result = orderServiceImpl.getAllOrders(new Order());
        assertEquals(testList,result);
    }


    @Test
    public void getAllOrdersWithExample(){
        List<Order> result = orderServiceImpl.getAllOrders((testOrder));
        assertEquals(testList,result);
    }


    @Test
    public void getAllOrdersUnexpectedError(){
        when(mockOrderRepository.findAll()).thenThrow();
        assertThrows(ServiceUnavailable.class,() -> orderServiceImpl.getAllOrders(new Order()));
    }


    @Test
    public void getOrdersByProductId(){
        List<Order> result = mockOrderRepository.findByItems_ProductId(2L);
        assertEquals(testList,result);
    }


    @Test
    public void getOrderByProductIdDBError(){
        when(mockOrderRepository.findByItems_ProductId(any(Long.class))).thenThrow(ServiceUnavailable.class);
        assertThrows(ServiceUnavailable.class,() -> orderServiceImpl.getOrdersByProductId(2L));
    }

    @Test
    public void getOrdersByQuantity(){
        List<Order> result = mockOrderRepository.findByItems_Quantity(10);
        assertEquals(testList,result);
    }

    @Test
    public void getOrderByQuantityDBError(){
        when(mockOrderRepository.findByItems_Quantity(any(Integer.class))).thenThrow(ServiceUnavailable.class);
        assertThrows(ServiceUnavailable.class,() -> orderServiceImpl.getOrdersByQuantity(10));
    }


    @Test
    public void getOrderById(){
        Order result = orderServiceImpl.getOrderById(1L);
        assertEquals(testOrder,result);
    }


    @Test
    public void getOrderDBError(){
        when(mockOrderRepository.findById(anyLong())).thenThrow(EmptyResultDataAccessException.class);
        assertThrows(ServiceUnavailable.class,() -> orderServiceImpl.getOrderById(1L));
    }


    @Test
    public void getOrderNotFound(){
        when(mockOrderRepository.findById(any(Long.class))).thenReturn(Optional.empty());
        assertThrows(ResourceNotFound.class,()-> orderServiceImpl.getOrderById(1L));
    }


    @Test
    public void addOrder(){
        Order result = orderServiceImpl.addOrder(testOrder);
        assertEquals(testOrder,result);
    }


    @Test
    public void addOrderDBError(){
        when(mockOrderRepository.save(any(Order.class))).thenThrow(ServiceUnavailable.class);
        assertThrows(ServiceUnavailable.class,() -> orderServiceImpl.addOrder(testOrder));
    }


    @Test
    public void addOrderCustomerNotFound(){
        when(mockCustomerRepository.existsById(any(Long.class))).thenReturn(false);

        assertThrows(ResourceNotFound.class,
                () -> orderServiceImpl.addOrder(testOrder));
    }


    @Test
    public void addOrderInvalidScale(){
        testOrder.setOrderTotal(BigDecimal.valueOf(999.999999));
        assertThrows(BadDataResponse.class,
                () -> orderServiceImpl.addOrder(testOrder));
    }


    @Test
    public void updateOrderById(){
        Order result = orderServiceImpl.updateOrderById(1L,testOrder);
        assertEquals(testOrder,result);
    }


    @Test
    public void updateOrderByIdDBError(){
        when(mockOrderRepository.save(any(Order.class))).thenThrow(ServiceUnavailable.class);
        assertThrows(ServiceUnavailable.class,() -> orderServiceImpl.updateOrderById(1L,testOrder));
    }



    @Test
    public void updateOrderByIdNotFound(){
        when(mockOrderRepository.findById(anyLong())).thenReturn(Optional.empty());
        assertThrows(ResourceNotFound.class,()-> orderServiceImpl.updateOrderById(1L,testOrder));
    }


    @Test
    public void updateOrderByIdInvalidScale(){
        testOrder.setOrderTotal(BigDecimal.valueOf(999.999999));
        assertThrows(BadDataResponse.class,
                () -> orderServiceImpl.updateOrderById(1L,testOrder));
    }


    @Test
    public void updateOrderBadData(){
        when(mockOrderRepository.save(any(Order.class))).thenReturn(testOrder);
        assertThrows(BadDataResponse.class,() -> orderServiceImpl.updateOrderById(2L,testOrder));
    }


    @Test
    public void deleteOrder(){
        when(mockOrderRepository.existsById(anyLong())).thenReturn(true);
        orderServiceImpl.deleteOrderById(1L);
        verify(mockOrderRepository).deleteById(any());
    }


    @Test
    public void deleteOrderDBError(){
        when(mockOrderRepository.existsById(anyLong())).thenReturn(true);
        doThrow(ServiceUnavailable.class).when(mockOrderRepository).deleteById(anyLong());
        assertThrows(BadDataResponse.class,() -> orderServiceImpl.deleteOrderById(1L));
    }


    @Test
    public void deleteOrderNotFound(){
        when(mockOrderRepository.existsById(anyLong())).thenReturn(false);
        assertThrows(ResourceNotFound.class,() -> orderServiceImpl.deleteOrderById(1L));
    }

}