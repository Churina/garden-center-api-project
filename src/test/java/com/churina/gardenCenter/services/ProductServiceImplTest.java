package com.churina.gardenCenter.services;

import com.churina.gardenCenter.entities.Product;
import com.churina.gardenCenter.exceptions.*;
import com.churina.gardenCenter.repositories.ProductRepository;
import com.churina.gardenCenter.repositories.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Example;
import org.springframework.transaction.CannotCreateTransactionException;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

class ProductServiceImplTest {

    @Mock
    ProductRepository mockProductRepository;

    @InjectMocks
    ProductServiceImpl productServiceImpl;

    // member variables for expected results
    Product testProduct;
    List<Product> testList = new ArrayList<>();

    @BeforeEach
    public void setUp(){
        // initialize mocks
        MockitoAnnotations.openMocks(this);

        // set up test product
        testProduct = new Product("a12345xyz","type1","name1","description1","manufacturer1", BigDecimal.valueOf(19.99));
        testProduct.setId(1L);
        testList.add(testProduct);

        when(mockProductRepository.findById(any(Long.class))).thenReturn(Optional.of(testList.get(0)));
        when(mockProductRepository.findAll()).thenReturn(testList);
        when(mockProductRepository.findAll(any(Example.class))).thenReturn(testList);
        when(mockProductRepository.saveAll(anyCollection())).thenReturn(testList);
        when(mockProductRepository.save(any(Product.class))).thenReturn(testList.get(0));
    }


    @Test
    public void getAllProducts(){
        List<Product> result = productServiceImpl.getAllProducts(new Product());
        assertEquals(testList,result);
    }


    @Test
    public void getAllProductsWithExample(){
        List<Product> result = productServiceImpl.getAllProducts(testProduct);
        assertEquals(testList,result);
    }


    @Test
    public void getAllProductsUnexpectedError(){
        when(mockProductRepository.findAll()).thenThrow();
        assertThrows(ServiceUnavailable.class,() -> productServiceImpl.getAllProducts(new Product()));
    }


    @Test
    public void getProductById(){
        Product result = productServiceImpl.getProductById(1L);
        assertEquals(testProduct,result);
    }


    @Test
    public void getProductDBError(){
        when(mockProductRepository.findById(anyLong())).thenThrow(EmptyResultDataAccessException.class);
        assertThrows(ServiceUnavailable.class,() -> productServiceImpl.getProductById(1L));
    }


    @Test
    public void getProductNotFound(){
        when(mockProductRepository.findById(any(Long.class))).thenReturn(Optional.empty());
        assertThrows(ResourceNotFound.class,() -> productServiceImpl.getProductById(1L));
    }



    @Test
    public void addProduct(){
        Product result = productServiceImpl.addProduct(testProduct);
        assertEquals(testProduct,result);
    }


    @Test
    public void addProductSkuConflict(){
        when(mockProductRepository.existsBySku(any(String.class))).thenReturn(true);
        assertThrows(Conflict.class,() -> productServiceImpl.addProduct(testProduct));
    }


    @Test
    public void addProductInvalidScale(){
        testProduct.setPrice(BigDecimal.valueOf(19.999999));
        assertThrows(BadDataResponse.class,() -> productServiceImpl.addProduct(testProduct));
    }

    @Test
    public void addProductDBError(){
        when(mockProductRepository.save(any(Product.class))).thenThrow(ServiceUnavailable.class);
        assertThrows(ServiceUnavailable.class,() -> productServiceImpl.addProduct(testProduct));
    }


    @Test
    public void updateProductById(){
        Product result = productServiceImpl.updateProductById(1L,testProduct);
        assertEquals(testProduct,result);
    }


    @Test
    public void updateProductSkuConflict(){
        when(mockProductRepository.existsBySku(any(String.class))).thenReturn(true);
        Product testProduct = new Product("b23456opt","type1","name1","description1","manufacturer1", BigDecimal.valueOf(19.99));
        testProduct.setId(1L);
        assertThrows(Conflict.class,() -> productServiceImpl.updateProductById(1L,testProduct));
    }


    @Test
    public void updateProductInvalidScale(){
        testProduct.setPrice(BigDecimal.valueOf(19.999999));
        assertThrows(BadDataResponse.class,() -> productServiceImpl.updateProductById(1L,testProduct));
    }


    @Test
    public void updateProductByIdDBError(){
        when(mockProductRepository.save(any(Product.class))).thenThrow(BadDataResponse.class);
        assertThrows(BadDataResponse.class,() -> productServiceImpl.updateProductById(1L,testProduct));
    }


    @Test
    public void updateProductByIdNotFound(){
        when(mockProductRepository.findById(anyLong())).thenReturn(Optional.empty());
        assertThrows(ResourceNotFound.class,() -> productServiceImpl.updateProductById(1L,testProduct));
    }


    @Test
    public void updateProductBadData(){
        when(mockProductRepository.save(any(Product.class))).thenReturn(testProduct);
        assertThrows(BadDataResponse.class,() -> productServiceImpl.updateProductById(2L,testProduct));
    }


    @Test
    public void deleteProduct(){
        when(mockProductRepository.existsById(anyLong())).thenReturn(true);
        productServiceImpl.deleteProductById(1L);
        verify(mockProductRepository).deleteById(any());
    }


    @Test
    public void deleteProductDBError(){
        when(mockProductRepository.existsById(anyLong())).thenReturn(true);
        doThrow(ServiceUnavailable.class).when(mockProductRepository).deleteById(anyLong());
        assertThrows(ServiceUnavailable.class,() -> productServiceImpl.deleteProductById(1L));
    }


    @Test
    public void deleteUserNotFound(){
        when(mockProductRepository.existsById(anyLong())).thenReturn(false);
        assertThrows(ResourceNotFound.class,() -> productServiceImpl.deleteProductById(1L));
    }

}