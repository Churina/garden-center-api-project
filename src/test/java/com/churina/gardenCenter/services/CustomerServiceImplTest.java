package com.churina.gardenCenter.services;

import com.churina.gardenCenter.entities.Address;
import com.churina.gardenCenter.entities.Customer;
import com.churina.gardenCenter.entities.User;
import com.churina.gardenCenter.exceptions.BadDataResponse;
import com.churina.gardenCenter.exceptions.Conflict;
import com.churina.gardenCenter.exceptions.ResourceNotFound;
import com.churina.gardenCenter.exceptions.ServiceUnavailable;
import com.churina.gardenCenter.repositories.CustomerRepository;
import com.churina.gardenCenter.repositories.UserRepository;
import org.checkerframework.checker.units.qual.C;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Example;
import org.springframework.test.context.TestPropertySource;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

class CustomerServiceImplTest {

    @Mock
    CustomerRepository mockCustomerRepository;

    @InjectMocks
    CustomerServiceImpl customerServiceImpl;

    // member variables for expected result
    Customer testCustomer;
    List<Customer> testList = new ArrayList<>();

    @BeforeEach
    public void setUp(){
        // initialize mocks
        MockitoAnnotations.openMocks((this));

        // set up test product
        testCustomer = new Customer("David","david@gmail.com",new Address("123 Street","Charlotte","NC","12345"));
        testCustomer.setId(1L);
        testList.add(testCustomer);

        when(mockCustomerRepository.findById(any(Long.class))).thenReturn(Optional.of(testList.get(0)));
        when(mockCustomerRepository.findAll()).thenReturn(testList);
        when(mockCustomerRepository.findAll(any(Example.class))).thenReturn(testList);
        when(mockCustomerRepository.saveAll(anyCollection())).thenReturn(testList);
        when(mockCustomerRepository.save(any(Customer.class))).thenReturn(testList.get(0));
    }


    @Test
    public void getAllCustomers(){
        List<Customer> result = customerServiceImpl.getAllCustomers(new Customer());
        assertEquals(testList,result);
    }


    @Test
    public void getAllCustomersWithExample(){
        List<Customer> result = customerServiceImpl.getAllCustomers(testCustomer);
        assertEquals(testList,result);
    }


    @Test
    public void getAllCustomersUnexpectedError(){
        when(mockCustomerRepository.findAll()).thenThrow();
        assertThrows(ServiceUnavailable.class,() -> customerServiceImpl.getAllCustomers(new Customer()));
    }


    @Test
    public void getCustomerById(){
        Customer result = customerServiceImpl.getCustomerById(1L);
        assertEquals(testCustomer,result);
    }


    @Test
    public void getCustomerDBError(){
        when(mockCustomerRepository.findById(anyLong())).thenThrow(EmptyResultDataAccessException.class);
        assertThrows(ServiceUnavailable.class,() -> customerServiceImpl.getCustomerById(1L));
    }


    @Test
    public void getCustomerNotFound(){
        when(mockCustomerRepository.findById(any(Long.class))).thenReturn(Optional.empty());
        assertThrows(ResourceNotFound.class,() -> customerServiceImpl.getCustomerById(1L));
    }


    @Test
    public void addCustomer(){
        Customer result = customerServiceImpl.addCustomer(testCustomer);
        assertEquals(testCustomer,result);
    }


    @Test
    public void addCustomerEmailConflict(){
        when(mockCustomerRepository.existsByEmail(any(String.class))).thenReturn(true);
        assertThrows(Conflict.class,() -> customerServiceImpl.addCustomer(testCustomer));
    }


    @Test
    public void addCustomerDBError(){
        when(mockCustomerRepository.save(any(Customer.class))).thenThrow(BadDataResponse.class);
        assertThrows(BadDataResponse.class,() -> customerServiceImpl.addCustomer(testCustomer));
    }


    @Test
    public void updateCustomerById(){
        Customer result = customerServiceImpl.updateCustomerById(1L,testCustomer);
        assertEquals(testCustomer,result);
    }


    @Test
    public void updateCustomerEmailConflict(){
        when(mockCustomerRepository.existsByEmail(any(String.class))).thenReturn(true);
        testCustomer = new Customer("David","emily@gmail.com",new Address("123 Street","Charlotte","NC","12345"));
        testCustomer.setId(1L);
        assertThrows(Conflict.class,() -> customerServiceImpl.updateCustomerById(1L, testCustomer));
    }


    @Test
    public void updateCustomerByIdDBError(){
        when(mockCustomerRepository.save(any(Customer.class))).thenThrow(BadDataResponse.class);
        assertThrows(BadDataResponse.class,() -> customerServiceImpl.updateCustomerById(1L, testCustomer));
    }


    @Test
    public void updateCustomerByIdNotFound(){
        when(mockCustomerRepository.findById(anyLong())).thenReturn(Optional.empty());
        assertThrows(ResourceNotFound.class,() -> customerServiceImpl.updateCustomerById(1L, testCustomer));
    }


    @Test
    public void updateCustomerBadData(){
        when(mockCustomerRepository.save(any(Customer.class))).thenReturn(testCustomer);
        assertThrows(BadDataResponse.class,() -> customerServiceImpl.updateCustomerById(2L,testCustomer));
    }


    @Test
    public void deleteCustomer(){
        when(mockCustomerRepository.existsById(anyLong())).thenReturn(true);
        customerServiceImpl.deleteCustomerById(1L);
        verify(mockCustomerRepository).deleteById(any());
    }


    @Test
    public void deleteCustomerDBError(){
        when(mockCustomerRepository.existsById(anyLong())).thenReturn(true);
        doThrow(ServiceUnavailable.class).when(mockCustomerRepository).deleteById(anyLong());
        assertThrows(ServiceUnavailable.class,() -> customerServiceImpl.deleteCustomerById(1L));
    }


    @Test
    public void deleteCustomerNotFound(){
        when(mockCustomerRepository.existsById(anyLong())).thenReturn(false);
        assertThrows(ResourceNotFound.class,() -> customerServiceImpl.deleteCustomerById(1L));
    }

}